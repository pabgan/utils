#!/bin/zsh

##########################
# Auxiliar functions
#

##########################
# 1. PARSE INPUT
zparseopts -F -D -verbose=o_verbose

help_message="Syntax is: $(basename $0) [--verbose] directory 'command'

		directory	is the directory in which git projects will be searched
		command		is the command that will be run: git pull, mvn clean install, whatever"

if [[ $# < 2 ]]; then
	echo ">ERROR> invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi

if [[ $o_verbose ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi

directory="$1"
command_to_execute="$2"

for d in ${directory-.}/**/.git/.. ; do
	(cd $d && 
		( echo ">INFO> $(pwd)" ;
		  eval $command_to_execute
		)
	)
done

