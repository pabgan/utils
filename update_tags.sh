#!/bin/zsh

##########################
# Auxiliary functions
#

create_command="ctags-universal ${language+--languages=${language}} -f ${tagfile} -R * --exclude='*/target/*' --exclude='*/.git/*'"
update_command="ctags-universal ${language+--languages=${language}} -a -f ${tagfile} -L <(echo ${updated_file}) --exclude='*/target/*' --exclude='*/.git/*'"
delete_tags_command="sed '/${updated_file}/d' -i ${tagfile_name}"

##########################
# 1. PARSE INPUT
zparseopts -F -D -verbose=verbose -language:=o_language -tags-file:=o_tag_file
verbose="${verbose[1]}"
language="${o_language[2]}"
tag_file="${o_tagfile[2]}"

help_message="Syntax is: $(basename $0) [--verbose] path"

if [[ $# != 1 ]]; then
	echo "ERROR: invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi
updated_file=$1
shift 1

if [[ $verbose ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi

# If this script is running, wait until it is not
#

# Error if no tag file found
if [[ -n ${tagfile} ]]; then
	if [[ ! -e ${tagfile} ]]; then
		echo ">ERROR> Could not find tag file ${tagfile}"
		exit 2
	fi
else
	tagfile="$(find_upwards.sh .tags)"
fi
if [[ ! -e ${tagfile} ]]; then
	echo ">ERROR> Could not automatically find .tags file"
	exit 2
fi

if [[ $verbose ]]; then echo ">DEBUG> Updating ${language} tags from ${updated_file} into ${tagfile}" >&2 ; fi

tagfile_name=$(basename $tagfile)
tagfile_path=$(dirname $tagfile)

( cd ${tagfile_path}
	# Delete tags related to file
	if [[ $verbose ]]; then echo ">DEBUG> executing: ${(e)delete_tags_command}" >&2 ; fi
	#eval ${delete_tags_command}

	# Create tags related to file
	if [[ $verbose ]]; then echo ">DEBUG> executing: ${(e)update_command}" >&2 ; fi
	#eval ${update_command}
)
