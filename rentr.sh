#!/bin/zsh

##########################
# Auxiliar functions
#

##########################
# 1. PARSE INPUT
zparseopts -F -D -verbose=o_verbose

help_message="Syntax is: $(basename $0) [--verbose] watchfor docommand

		watchfor	Command that list files to look for
		docommand	Command to be executed when the list or contents of a file in the list changes"

if [[ $# < 2 ]]; then
	echo ">ERROR> invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi

if [[ $o_verbose ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi


entrr() {
	watchfor="$1"
	watchlist=$(eval ${(ze)watchfor})
	if [[ $o_verbose ]]; then echo ">DEBUG> watchlist=${watchlist}" >&2 ; fi
	comm="$2"
	echo $watchlist | entr -cd "${(ze)comm}"
	if [[ $? > 0 ]]
	then
			echo ">INFO> Launching entrr again"
			entrr "$watchfor" "$comm"
	fi
}

entrr $@
