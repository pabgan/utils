#!/usr/bin/env python3
import os, logging, argparse
import sys

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def tuplist2str(tuplist):
    s = [[str(e).strip() for e in row] for row in tuplist]
    logger.debug("s={}.".format(s))
    lens = [max(map(len, col)) for col in zip(*s)]
    s.insert(1, ['-'*l for l in lens])
    fmt = '|'.join('{{{0}:{1}}}'.format(i, x) for i, x in enumerate(lens))
    table = ['|' + fmt.format(*row) + '|' for row in s]

    return '\n'.join(table)


def strlist2tuplist(strlist):
    tuplist = []
    separator=' '

    if '\t' in strlist[0]:
        logger.debug("Will use tabs as separator.")
        separator='\t'
    elif ';' in strlist[0]:
        logger.debug("Will use ; as separator.")
        separator=';'
    else:
        logger.debug("Will use spaces as separator.")

    for str in strlist:
        # Avoid empty lines
        if str.strip():
            tuplist.append(tuple(str.strip().split(separator)))

    return tuplist


if __name__ == '__main__':
    ###########################################################
    # Parse arguments
    desc  = "Convert a table into a markdown table, smartly!"
    desc += "It detects tabs, semicolons or spaces as column separators, in that order."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("--log", help="Select log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)")
    args = parser.parse_args()

    if args.log:
        logger.setLevel(args.log)
        logger.debug("Setting log level to {}.".format(args.log))

    input = sys.stdin.readlines()

    if input is None:
        sys.exit("Missing input.")

    logger.debug("input={}".format(input))

    tuplist = strlist2tuplist(input)
    logger.debug("tuplist={}".format(tuplist))

    output = tuplist2str(tuplist)

    print(output)

