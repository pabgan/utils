#!/bin/zsh

##########################
# 0. Auxiliary functions
#

##########################
# 1. PARSE INPUT
zparseopts -D -verbose=o_verbose -all=o_all

help_message="Syntax is: $(basename $0) [-verbose] file_name"

if [[ $# < 1 ]]; then
	echo ">ERROR> invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi
file_name=$1

if [[ $o_verbose ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi

check_dir() {
	current_path=$(pwd)

	if [[ $o_verbose ]]; then echo ">DEBUG> Checking '$current_path'" >&2 ; fi

	if [[ -e $file_name ]]; then
		echo "$current_path/$file_name"
		if [[ ! $o_all ]]; then
			if [[ $o_verbose ]]; then echo ">DEBUG> Stopping there." >&2 ; fi
			exit
		fi
		if [[ $o_verbose ]]; then echo ">DEBUG> Looking for more." >&2 ; fi
	elif [[ $current_path == '/' ]]; then
		exit
	fi
	(cd .. && check_dir $file_name)
}

check_dir
