#!/bin/zsh

##########################
# 1. PARSE INPUT
zparseopts -F -D -verbose=verbose
verbose="${verbose[1]}"

if [[ $verbose ]]; then echo ">DEBUG> Verbose mode" >&2 ; fi

# Clean apt cache of installed packages
echo ">INFO> Cleaning apt cache" >&2 
sudo apt clean

# Clean normal logs
echo ">INFO> Forcing rotation of logs" >&2 
sudo logrotate /etc/logrotate.d/*

echo ">INFO> Removing compressed old logs" >&2 
sudo rm /var/log/*.gz

# Clean systemd logs
echo ">INFO> Cleaning systemd logs older than 1 week" >&2 
sudo journalctl --vacuum-time=1w

# Clean flatpak files
echo ">INFO> Removing unused flatpak files" >&2 
sudo flatpak uninstall --unused
